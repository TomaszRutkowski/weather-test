//
//  DetailDataSource.h
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DetailDataSource : NSObject <UITableViewDataSource>

@property (strong, nonatomic) NSDictionary *timestampWeather;

- (float)KelvinToCelsius:(float)kelvin;

@end
