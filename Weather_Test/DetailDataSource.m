//
//  DetailDataSource.m
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import "DetailDataSource.h"

@implementation DetailDataSource

- (float)KelvinToCelsius:(float)kelvin
{
    return kelvin - 273.15;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell" forIndexPath:indexPath];
    NSDictionary *mainDict = self.timestampWeather[@"main"];
    switch (indexPath.row)
    {
        case 0:
        {
            NSNumber *kelvins = mainDict[@"temp"];
            NSNumber *celsius = [NSNumber numberWithInt:roundf([self KelvinToCelsius:[kelvins floatValue]])];
            [cell.textLabel setText: @"Temperature *C"];
            [cell.detailTextLabel setText:[celsius stringValue]];
        }
        break;
            
        case 1:
        {
            NSNumber *hpaPressure = mainDict[@"pressure"];
            [cell.textLabel setText:@"Pressure hPa"];
            [cell.detailTextLabel setText:[hpaPressure stringValue]];
        }
        break;
            
        case 2:
        {
            NSNumber *humidity = mainDict[@"humidity"];
            [cell.textLabel setText:@"Humidity %"];
            [cell.detailTextLabel setText:[humidity stringValue]];
        }
        break;
            
        default:
        //shouldn't happen
        break;
    }
    
    return cell;
}

@end
