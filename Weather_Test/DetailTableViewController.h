//
//  DetailTableViewController.h
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailDataSource.h"

@interface DetailTableViewController : UITableViewController

@property (strong, nonatomic) NSDictionary *timestampWeather;
@property (strong, nonatomic) DetailDataSource *detailDataSource;

@end
