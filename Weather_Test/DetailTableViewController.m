//
//  DetailTableViewController.m
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import "DetailTableViewController.h"

@interface DetailTableViewController ()

@end

@implementation DetailTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.detailDataSource = [[DetailDataSource alloc] init];
    self.detailDataSource.timestampWeather = self.timestampWeather;
    self.tableView.dataSource = self.detailDataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
