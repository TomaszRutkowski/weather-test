//
//  OverviewDataSource.h
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OverviewDataSource : NSObject <UITableViewDataSource>

@property (strong, nonatomic) NSArray *weatherList;

@end
