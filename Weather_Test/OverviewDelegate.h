//
//  OverviewDelegate.h
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DetailTableViewController.h"

@interface OverviewDelegate : NSObject <UITableViewDelegate>

@property (weak, nonatomic) UIViewController *parent;

@end
