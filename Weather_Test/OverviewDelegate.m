//
//  OverviewDelegate.m
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import "OverviewDelegate.h"

@implementation OverviewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.parent performSegueWithIdentifier:@"OverviewToDetail" sender:self];
}

@end
