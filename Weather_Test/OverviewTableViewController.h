//
//  OverviewTableViewController.h
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WeatherJSONManager.h"
#import "OverviewDataSource.h"
#import "OverviewDelegate.h"


@interface OverviewTableViewController : UITableViewController <WeatherJSONDelegate>

@property (strong, nonatomic) MBProgressHUD *loading;
@property (strong, nonatomic) NSDictionary *weatherData;
@property (strong, nonatomic) OverviewDelegate *overviewDelegate;
@property (strong, nonatomic) OverviewDataSource *overviewDataSource;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *reloadButton;

- (IBAction)reload:(id)sender;

@end
