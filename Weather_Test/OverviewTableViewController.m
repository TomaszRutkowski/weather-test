//
//  OverviewTableViewController.m
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import "OverviewTableViewController.h"

@interface OverviewTableViewController ()

@end

@implementation OverviewTableViewController

- (void)attemptLoad
{
    self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.loading.mode = MBProgressHUDModeIndeterminate;
    self.loading.label.text = @"Loading Weather";
    [WeatherJSONManager sharedWeatherJSONManager].delegate = self;
    [[WeatherJSONManager sharedWeatherJSONManager] getWeatherJSON];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self attemptLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqual: @"OverviewToDetail"])
    {
        DetailTableViewController *dTVC = segue.destinationViewController;
        NSArray *arrayOfWeather = self.weatherData[@"list"];
        dTVC.timestampWeather = [arrayOfWeather firstObject];
        dTVC.navigationItem.title = [self.tableView cellForRowAtIndexPath:self.tableView.indexPathForSelectedRow].textLabel.text;
    }
}

- (void)SuccessfulLoad:(NSDictionary *)weatherJSON
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self.loading hideAnimated:YES];
        self.weatherData = weatherJSON;
        if (self.weatherData[@"list"] != nil)
        {
            self.overviewDataSource = [[OverviewDataSource alloc] init];
            self.overviewDataSource.weatherList = self.weatherData[@"list"];
            self.tableView.dataSource = self.overviewDataSource;
            self.overviewDelegate = [[OverviewDelegate alloc] init];
            self.overviewDelegate.parent = self;
            self.tableView.delegate = self.overviewDelegate;
            [self.tableView reloadData];
        }
        else
        {
            [self FailedLoad:@"No list found in data."];
        }
    });
}

- (void)FailedLoad:(NSString *)errorDescription
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self.loading hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Failed To Load" message:errorDescription preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    });
}


- (IBAction)reload:(id)sender
{
    [self attemptLoad];
}

@end
