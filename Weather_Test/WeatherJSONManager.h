//
//  WeatherJSONManager.h
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WeatherJSONDelegate <NSObject>

@required

- (void)SuccessfulLoad:(NSDictionary *)weatherJSON;
- (void)FailedLoad:(NSString *)errorDescription;

@end

@interface WeatherJSONManager : NSObject

@property (weak, nonatomic) id<WeatherJSONDelegate> delegate;

+ (instancetype) sharedWeatherJSONManager;
- (void) getWeatherJSON;

@end
