//
//  WeatherJSONManager.m
//  Weather_Test
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import "WeatherJSONManager.h"

@implementation WeatherJSONManager

+ (instancetype) sharedWeatherJSONManager
{
    static WeatherJSONManager *sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (void) parseWeatherJSONWithData:(NSData *)jsonData
{
    NSError *error;
    NSDictionary *root = [NSJSONSerialization JSONObjectWithData:(NSData *)jsonData options:0 error:&error];
    if (!error)
    {
        [self.delegate SuccessfulLoad:root];
    }
    else
    {
        [self.delegate FailedLoad:@"Failed to parse JSON."];
    }
}

- (void) getWeatherJSON
{
    if (self.delegate != nil)
    {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://api.openweathermap.org/data/2.5/forecast?q=London,uk&mode=json&APPID=e7ae82902ea67de1d5d7667173daded4"]];
        NSURLSessionDataTask *download = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
                                      ^(NSData * data, NSURLResponse * response, NSError * error){
                                          if (error)
                                          {
                                              [self.delegate FailedLoad:error.localizedDescription];
                                          }
                                          else if (data)
                                          {
                                              [self parseWeatherJSONWithData:data];
                                          }
                                          else
                                          {
                                              [self.delegate FailedLoad:@"No data found."];
                                          }
                                      }];
        [download resume];
    }
}

@end
