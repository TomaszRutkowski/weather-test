//
//  Weather_TestTests.m
//  Weather_TestTests
//
//  Created by Tom Rutkowski on 4/13/17.
//  Copyright © 2017 Tom_Rutkowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DetailDataSource.h"

@interface Weather_TestTests : XCTestCase

@end

@implementation Weather_TestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testKelvinToCelsiusConversion
{
    DetailDataSource *dds = [[DetailDataSource alloc] init];
    float absoluteZero = [dds KelvinToCelsius: 0.0];
    XCTAssertEqual(roundf(absoluteZero), -273);
    float roomTemperature = [dds KelvinToCelsius: 293.0];
    XCTAssertEqual(roundf(roomTemperature), 20);
    float someRandomTemperature = [dds KelvinToCelsius: 287.0];
    XCTAssertEqual(roundf(someRandomTemperature), 14);
}

@end
