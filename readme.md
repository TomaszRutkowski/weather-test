# Weather Test App

## Setting up the App from the repository

* In order to run this repository, you will need to use CocoaPods to fetch the MBProgressHud via the included Podfile.
* Use the instructions on the CocoaPods website: https://guides.cocoapods.org/using/index.html 

## Running the App

* To run the app in Xcode, select the device you wish to use in the top left hand corner before pressing the run button. The run button will also automatically build the app.
* To run the app on a real device, hit the app shortcut for “Weather_Test”

## Building the app

* To build the app in Xcode without running it, select on the menu bar: **Product** -> **Build**

## Running the Unit Tests

* To run the tests in Xcode, hold click on the run button to make a drop-down menu appear. Select the “Test” option that appears, with the icon of a spanner.

## Potential Improvements

* More unit tests!
* More nil checks in case API undergoes a structure change, to make app future proof.
* Delimiting days into sections, so that rows only need to hold the time, to make it look neater.
* Better date formating.
* More data in the detail screen.
* Search screen as root view to allow different cities to be selected.
* Use of icon thumbnails in the main screen (icons provided by API).
* Visual styling on MBProgressHUD.
* Caching data and storing using Core Data.
